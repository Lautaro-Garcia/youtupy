from os.path import exists, expanduser
import yaml


class Configuration:
    """
    Se encarga de buscar la configuración en el path default.
    Tiene ciertos campos por defecto.
    Si no encuentra un campo en el archivo de configuración
    devuelve None
    """
    def __init__(self):
        self.confFile = expanduser('~/.config/youtupy.yml')
        self.actualConfiguration = {
            'host': '',
            'port': 8080,
            'connectionLimit': 10,
            'temporaryAudioDir': '/tmp',
        }
        self._load()

    def _load(self):
        if exists(self.confFile):
            conf = yaml.load(open(self.confFile))
            if type(conf) == dict:
                self.actualConfiguration.update(conf)

    def __getattr__(self, attrName):
        return self.actualConfiguration.get(attrName)
