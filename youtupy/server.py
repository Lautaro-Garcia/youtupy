import socket
import sys
from termcolor import cprint
from threading import Thread


class Server:
    def __init__(self, player, configuration):
        self.player = player
        self.host = configuration.host
        self.port = configuration.port
        self.listeningConnectionLimit = configuration.connectionLimit
        self._getUpAndListening()

    def _getUpAndListening(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.host, self.port))
        self.socket.listen(self.listeningConnectionLimit)

    def _receiveLink(self, connection):
        link = connection.recv(1024)
        self.player.addToQueue(link.decode())

    def run(self):
        cprint("Servidor escuchando en el puerto %s" % self.port, "grey")
        while True:
            try:
                conn, addr = self.socket.accept()
                Thread(target=self._receiveLink, args=(conn,)).start()
            except KeyboardInterrupt:
                self.socket.close()
                cprint("\nEl servidor terminó correctamente", 'green')
                sys.exit()
