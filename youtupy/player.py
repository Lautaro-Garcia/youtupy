import os
import subprocess
from termcolor import cprint


class Player:
    def __init__(self, downloader):
        self.downloader = downloader
        self.queue = []
        self.isPlaying = False

    def _play(self):
        self.isPlaying = True
        audioFilename = self.queue.pop()
        cprint('Reproduciendo una nueva canción', 'green', attrs=['bold'])
        subprocess.run(['mplayer', audioFilename], stdout=subprocess.DEVNULL,
                       stderr=subprocess.DEVNULL)
        os.remove(audioFilename)
        if self.queue:
            self._play()
        else:
            cprint('Cola de reproducción vacía', 'magenta', 'on_white')
            self.isPlaying = False

    def addToQueue(self, link):
        audioFilename = self.downloader.download(link)
        if audioFilename:
            self.queue.append(audioFilename)
            if not self.isPlaying:
                self._play()
