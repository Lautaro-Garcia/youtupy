import os
import subprocess
from termcolor import cprint
import uuid


class Downloader:
    def __init__(self, configuration):
        self.tempDir = configuration.temporaryAudioDir

    def download(self, link):
        tempname = os.path.join(self.tempDir, str(uuid.uuid4()))
        command = ['youtube-dl', '-f', 'bestaudio', link, '-o', tempname]
        cprint('Descargando el archivo %s' % link, 'yellow')
        status = subprocess.run(command, stdout=subprocess.DEVNULL,
                                stderr=subprocess.DEVNULL)
        if not status.returncode:
            cprint('%s correctamente descargado' % link, 'green')
            return tempname
        cprint('El link %s es inválido' % link, 'red', attrs=['blink'])
