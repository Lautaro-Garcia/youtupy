import socket
from termcolor import cprint


class Client:
    def __init__(self, host='localhost', port=8080):
        self.host = host
        self.port = int(port)
        self.socket = socket.create_connection((self.host, self.port))

    def askForVideo(self, link):
        self.socket.sendall(link.encode())
        cprint('El video %s fue enviado al servidor' % link, 'green')
