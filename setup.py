from setuptools import setup

setup(name='youtupy',
      version='0.1',
      packages=['youtupy'],
      install_requires=['pyyaml', 'termcolor'],
      scripts=['bin/youtupyClient', 'bin/youtupyServer'],
      include_package_data=True,
      license='MIT')
