# Youtupy

Pequeñísimo programa en Python que permite tener un servidor que descarga y
reproduce videos de Youtube. Cuenta con un cliente mucho más simple que
puede enviar links de Youtube para encolar en los videos a reproducir.

## Dependencias(sólo para el servidor)
* Para descargar el video de youtube se usa youtube-dl
* Para reproducir el audio se usa mplayer
  _Creo igual que mplayer es dependencia de youtube-dl, pero para se
  totalmente sincero con los programas que usa, lo listo_

## Uso
El servidor no requiere de más trabajo que ejecutar:
`
$ youtupyServer
`

La configuración del server tiene que hacerse en formato YAML. El directorio
de esta configuración es `~/.config/youtupy.yml`
Actualmente se pueden configurar los siguientes campos

 * __host__ [default localhost]
 * __port__ [default 8080]
 * __connectionLimit__ [cantidad de conexiones simultáneas que soporta el server | default 10]
 * __temporaryAudioDir__ [dónde deja los audios temporales para escuchar | default /tmp]

El cliente se usa de la siguiente manera:
`
$ youtupy <host> <puerto> <link a añadir>
`
